set number
set showmatch
set tabstop=4
set softtabstop=4
set expandtab
set showcmd
set cursorline
set wildmenu
set lazyredraw
set showmatch
set incsearch
set hlsearch
filetype indent on
syntax on
colorscheme monokai

