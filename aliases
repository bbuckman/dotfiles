#Shell Aliases
alias cat='bat'
alias ls='exa'
alias ll='ls -alF --color=auto --human-readable --group-directories-first --classify'                                                                                               
alias la='ls -A'
alias l='ls -CF'
alias cp='cp -iv'
alias mv='mv -iv'
alias rmdir='rmdir -v'
alias ln='ln -v'
alias grep='grep --color=auto'
alias vmstat='vmstat -w'
alias ping='grc ping'
alias ifconfig='grc ifconfig'
alias netstat='grc netstat'
alias ps='grc ps'

## Git Aliases
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias go='git checkout '
alias gk='gitk --all&'
alias gx='gitx --all'
alias got='git '
alias get='git '
alias gdiff='git difftool -y -x "colordiff -y -W $COLUMNS" | less -R'

## Vagrant Aliases
alias vag='vagrant'
alias vagup='vagrant up'
alias vagdestroy='vagrant destroy'                                                                                    
alias vagssh='vagrant ssh'
alias vaghalt='vagrant halt'

## TCPdump aliases
alias dumppwd="sudo tcpdump port http or port ftp or port smtp or port imap or port pop3 or port telnet -lA | egrep -i -B5 'pass=|pwd=|log=|login=|user=|username=|pw=|passw=|passwd=|password=|pass:|user:|username:|password:|login:|pass |user '"
alias dumpuseragent="sudo tcpdump -vvAls0 | grep 'User-Agent:'"
alias dumpgetreq="sudo tcpdump -vvAls0 | grep 'GET'"

##Miscellaneous Aliases
alias htop='sudo htop'
alias cat='bat'
alias fx='firefox --new-instance --profile $(mktemp -d)'
alias csv='column -s, -t'
alias changedfiles="find . -type f -print0 | xargs -0 stat --format '%Z :%z %n' | sort -nr | cut -d: -f2- | head -n 20"
alias ports='grc netstat -tulpena'
alias dtail='dmesg|tail'
alias psg='sudo grc ps -ef | grep -i $1'
alias nsg='sudo grc netstat -natp | grep -i $1'

#functions
##qqfind - used to quickly find files that contain a string in a directory
function qfind () {  find . -exec grep -l -s $1 {} \;  return 0}

##weather - gets weather in $1 zip
function weather() {curl wttr.in/$1}

##cheat - cht.sh cheatsheet of various Unix commands
function cheat()  {curl cht.sh/$1}

## make directory and enter it
function mdc() { mkdir -p "$@" && cd "$@"; }

##cd up a specified number of times
function up () {
        local tmp_path=''
        for i in $(seq 1 ${1:-1})
        do
            tmp_path+='../'
        done
        cd "$tmp_path"
    }

## # Display PKI chain-of-trust for a given domain
function certchain() {
if [[ "$#" -ne 1 ]]; then
          echo "Usage: ${FUNCNAME} <ip|domain[:port]>"
          return 1
      fi

      local host_port="$1"

      if [[ "$1" != *:* ]]; then
          local host_port="${1}:443"
      fi

      openssl s_client -connect "${host_port}" </dev/null 2>/dev/null | grep -E '\ (s|i):'
  }

## create a file and make git track it
function gtouch() { touch "$@" && git add "$@" }

##extract - finds a function to extract a file
function extract () {
      if [ -f $1 ] ; then
        case $1 in
          *.tar.bz2)   tar xjf $1     ;;
          *.tar.gz)    tar xzf $1     ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       unrar e $1     ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xf $1      ;;
          *.tbz2)      tar xjf $1     ;;
          *.tgz)       tar xzf $1     ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *)     echo "'$1' cannot be extracted via extract()" ;;
           esac
       else
           echo "'$1' is not a valid file"
       fi
     }

##histgrep - grep the history file
function histgrep () 
  {
  grep -r "$@" ~/.history
  history | grep "$@"
  }

# Custom exports
## Set EDITOR to /usr/bin/vim if Vim is installed
if [ -f /usr/bin/vim ]; then
  export EDITOR=/usr/bin/vim
fi





