
Dotfiles Installer
======================

Largelyy a ripoff of https://github.com/michaeljsmalley/dotfiles.  Thanks Michael!

See Michael's readme at https://github.com/michaeljsmalley/dotfiles/blob/master/README.markdown

Installation
=======================
git clone https://bitbucket.org/0xBDB/dotfiles.git ~/dotfiles  
cd ~/dotfiles  

--At the moment it's recessary to manually install oh-my-zsh because I need to work on submodule linking  
cd dotfiles  
rm -rf oh-my-zsh/  
git clone https://github.com/robbyrussell/oh-my-zsh    
sudo apt-get install fonts-powerline fzf grc  
Get bat from https://github.com/sharkdp/bat  
  
./install.sh  
reboot  

Things I've added
-----------------------

Additional aliases, change to my preferred zsh theme, etc.  For some of the alaises to work you will need to install fzf, grc, bat.

Things to add
-----------------------

Ansible integration to install a much larger set of tools, particularly for pentesters.


